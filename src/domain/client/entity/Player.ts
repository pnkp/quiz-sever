export class Player {
	playerName: string;
	id: string;
	room: string;
	ready?: boolean;
}
