import { Player } from "./Player";

export class UserWithCategory {
	user: Player;
	category: string;
	room: string;
}
