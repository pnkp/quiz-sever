export class RoomExistsException extends Error {
	constructor(roomName: string) {
		super(`Room name exists. Room name given: ${roomName}`);
	}
}
