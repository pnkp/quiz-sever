import { inject, injectable } from "inversify";
import { UserEntity } from "../../infrastructure/persist/client/entity/UserEntity";
import { UserCategoryRepository } from "../../infrastructure/persist/client/UserCategoryRepository";
import { UserRepository } from "../../infrastructure/persist/client/UserRepository";
import { Player } from "./entity/Player";
import { UserWithCategory } from "./entity/UserWithCategory";
import { CLIENT_TYPES } from "./ioc/ClientTypes";

@injectable()
export class UserService {

	@inject(CLIENT_TYPES.UserRepository)
	private readonly userRepository: UserRepository;

	@inject(CLIENT_TYPES.UserCategoryRepository)
	private readonly userCategoryRepository: UserCategoryRepository;

	public addUserAndJoinToRoom(user: Player): Player {
		const userEntity: UserEntity = Object.assign({}, {
			...user
		});

		const savedUserEntity = this.userRepository.add(userEntity);

		return {
			playerName: savedUserEntity.playerName,
			id: savedUserEntity.id,
			room: savedUserEntity.room
		};
	}

	public getAllUniqRooms(): string[] {
		const rooms = this.userRepository.findAllExistsRooms();

		const uniqRooms = new Set(rooms);
		return Array.from(uniqRooms);
	}

	public addUserToSelectedCategory(userId: string, category: string): UserWithCategory {
		const user: Player = this.userRepository.findOne(userId);

		const userEntity: UserEntity = Object.assign({}, {
			...user
		});

		const savedUserWithCategory = this.userCategoryRepository.addUserToCategory(userEntity, category);

		return {
			user: savedUserWithCategory.user,
			room: savedUserWithCategory.room,
			category: savedUserWithCategory.category
		}
	}

	public removeUser(userId: string): Player {
		const user: UserEntity = this.userRepository.findOne(userId);

		this.userRepository.remove(user.id);

		return {
			id: user.id,
			room: user.room,
			playerName: user.playerName,
		}
	}

	public findUser(userId: string): Player {
		const user: UserEntity = this.userRepository.findOne(userId);

		return  {
			playerName: user.playerName,
			id: user.id,
			room: user.room
		};
	}
}
