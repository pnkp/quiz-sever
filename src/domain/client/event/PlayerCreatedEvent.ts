import { PlayerDto } from "../../../application/client/PlayerDto";

export class PlayerCreatedEvent {
	get player(): PlayerDto {
		return this._player;
	}

	constructor(private readonly _player: PlayerDto) {
	}
}
