export const CLIENT_TYPES = {
	UserService: Symbol.for('UserService'),
	CategorySelectorQuery: Symbol.for('CategorySelectorQuery'),
	UserCategoryRepository: Symbol.for('UserCategoryRepository'),
	UserRepository: Symbol.for('UserRepository'),
	PlayersQuery: Symbol.for('PlayersQuery'),
	CreatePlayerCommand: Symbol.for('CreatePlayerCommand'),
	RoomRepository: Symbol.for('RoomRepository'),
	CreateRoomCommandValidator: Symbol.for('CreateRoomCommandValidator'),
	CreateRoomCommand: Symbol.for('CreateRoomCommand'),
	RemovePlayerCommand: Symbol.for('RemovePlayerCommand'),
};
