import { inject, injectable } from "inversify";
import { LevelEntity } from "../../infrastructure/persist/game/entity/LevelEntity";
import { LevelRepository } from "../../infrastructure/persist/game/LevelRepository";
import { CategoryChooser } from "./CategoryChooser";
import { Level } from "./entity/Level";
import { CATEGORY_TYPES } from "./ioc/CategoryTypes";
import { GAME_TYPES } from "./ioc/GameTypes";

@injectable()
export class LevelService {

	@inject(CATEGORY_TYPES.CategoryChooser)
	private readonly categoryChooser: CategoryChooser;

	@inject(GAME_TYPES.LevelRepository)
	private readonly levelRepository: LevelRepository;

	public createLevel(): Level {
		const randomlySelectedCategories: string[] = this.categoryChooser.getRandomCategories();

		const level: LevelEntity = {
			levelFinished: false,
			randomlySelectedCategories,
			categorySelectedByUser: []
		};

		this.levelRepository.create(level);

		return {
			levelFinished: level.levelFinished,
			randomlySelectedCategories: level.randomlySelectedCategories,
			categorySelectedByUser: level.categorySelectedByUser,
			id: level._id
		}
	}

}
