export const GAME_TYPES = {
	GameRepository: Symbol.for('GameRepository'),
	CreateGameCommand: Symbol.for('CreateGameCommand'),
	LevelRepository: Symbol.for('LevelRepository'),
	LevelService: Symbol.for('LevelService'),
	GameService: Symbol.for('GameService')
};
