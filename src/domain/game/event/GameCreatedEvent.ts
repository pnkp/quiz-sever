import { Game } from "../entity/Game";

export class GameCreatedEvent {
	get game(): Game {
		return this._game;
	}
	constructor(private readonly _game: Game) {
	}
}
