import { inject, injectable } from "inversify";
import { UserRepository } from "../../infrastructure/persist/client/UserRepository";
import { GameEntity } from "../../infrastructure/persist/game/entity/GameEntity";
import { LevelEntity } from "../../infrastructure/persist/game/entity/LevelEntity";
import { GameRepository } from "../../infrastructure/persist/game/GameRepository";
import { CLIENT_TYPES } from "../client/ioc/ClientTypes";
import { Game } from "./entity/Game";
import { GAME_TYPES } from "./ioc/GameTypes";

@injectable()
export class GameService {

	@inject(GAME_TYPES.GameRepository)
	private readonly gameRepository: GameRepository;

	@inject(CLIENT_TYPES.UserRepository)
	private readonly userRepository: UserRepository;

	public createGame(socketId: string): Game {
		const user = this.userRepository.findOne(socketId);
		const users = this.userRepository.findUsersInRoom(user.room);

		const newGame: GameEntity = {
			room: user.room,
			level: null,
			users
		};

		const game = this.gameRepository.create(newGame);

		return {
			room: game.room,
			level: null,
			users: game.users
		};
	}

	public removePlayerFromGame(game: Game, socketId: string): Game {
		const userIndex = game?.users.findIndex(u => u.id === socketId);
		game?.users.splice(userIndex, 1);

		this.gameRepository.update(game);

		return game;
	}

	public updateGameState(game: Game): Game {
		const levelEntity: LevelEntity = {
			_id: game.level.id,
			randomlySelectedCategories: game.level.randomlySelectedCategories,
			levelFinished: game.level.levelFinished,
			categorySelectedByUser: game.level?.categorySelectedByUser
		};

		const gameEntity: GameEntity = {
			...game,
			level: levelEntity
		};

		this.gameRepository.update(gameEntity);

		return {
			level: gameEntity.level,
			room: gameEntity.room,
			users: gameEntity.users
		}
	}

}
