import { Player } from "../../client/entity/Player";
import { Level } from "./Level";

export class Game {
	public room: string;
	public users: Player[];
	public level: Level;
}
