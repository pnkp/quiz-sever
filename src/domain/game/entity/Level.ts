import { Player } from "../../client/entity/Player";

export class Level {
	id?: string;
	randomlySelectedCategories: string[];
	categorySelectedByUser?: Array<{user: Player; category: string}>;
	levelFinished: boolean;
}
