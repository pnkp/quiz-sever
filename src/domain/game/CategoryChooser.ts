import fs from "fs";
import { injectable } from "inversify";
import { maxBy } from "lodash";

@injectable()
export class CategoryChooser {

	public selectCategory() {

	}

	public getMostSelectedCategory(categoryGroupedBySelections: Array<[string, number]>): string {
		const highestCountOfSelectedCategory: [string, number] = maxBy(categoryGroupedBySelections, '1');
		const mostSelectedCategory: Array<[string, number]> = categoryGroupedBySelections.filter(c => c[1] === highestCountOfSelectedCategory[1]);

		if (mostSelectedCategory.length === 1) {
			return highestCountOfSelectedCategory[0]
		}

		const randomCategoryIndex = this.getRandomInt(mostSelectedCategory.length - 1);

		return mostSelectedCategory[randomCategoryIndex][0];
	}

	public getRandomCategories(): string[] {
		const content = fs.readFileSync("questions/categories.json");
		const categories: string[] = JSON.parse(content.toString());

		const randomNumbers: number[] = this.getRandomNumbersForSelectCategories(categories.length);

		return categories.filter((v, index) => randomNumbers.includes(index));
	}

	private getRandomInt(max: number): number {
		const min = 0;
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	private getRandomNumbersForSelectCategories(countOfAllAvailableCategories: number): number[] {
		const countCategoryToSelect: number = Number.parseInt(process.env.COUNT_CATEGORY_TO_SELECT, 10);
		const arr = [];

		while(arr.length <= countCategoryToSelect){
			const r = Math.floor(Math.random() * countOfAllAvailableCategories) + 1;
			if(arr.indexOf(r) === -1) arr.push(r);
		}

		return arr;
	}
}
