import { inject, injectable } from "inversify";
import { countBy, isNil } from "lodash";
import { CategoryChooser } from "../../../domain/game/CategoryChooser";
import { CATEGORY_TYPES } from "../../../domain/game/ioc/CategoryTypes";
import { GAME_TYPES } from "../../../domain/game/ioc/GameTypes";
import { SelectedCategoryEnum } from "../../../domain/game/SelectedCategoryEnum";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserCategoryRepository } from "../../../infrastructure/persist/client/UserCategoryRepository";
import { UserRepository } from "../../../infrastructure/persist/client/UserRepository";
import { GameRepository } from "../../../infrastructure/persist/game/GameRepository";
import { CategorySelectorDto } from "../../client/CategorySelectorDto";

@injectable()
export class CategorySelectorQuery {
	@inject(CLIENT_TYPES.UserRepository)
	private readonly userRepository: UserRepository;

	@inject(CLIENT_TYPES.UserCategoryRepository)
	private readonly userCategoryRepository: UserCategoryRepository;

	@inject(CATEGORY_TYPES.CategoryChooser)
	private readonly categoryChooser: CategoryChooser;

	@inject(GAME_TYPES.GameRepository)
	private readonly gameRepository: GameRepository;

	public getMostSelectedCategoryByUsers(userRoom: string): CategorySelectorDto {
		const usersInRoom = this.userRepository.findUsersInRoom(userRoom);
		const usersWithSelectedCategory = this.userCategoryRepository.findUsersInRoomWithSelectedCategory(userRoom);

		if (usersInRoom.length !== usersWithSelectedCategory.length) {
			return {
				allUsersSelectedCategory: SelectedCategoryEnum.no
			}
		}

		const countedSelectedCategory: Array<[string, number]> =
				Array.from(Object.entries(countBy(usersWithSelectedCategory, 'category')));

		const selectedCategory: string = this.categoryChooser.getMostSelectedCategory(countedSelectedCategory);

		return {
			allUsersSelectedCategory: SelectedCategoryEnum.all,
			selectedCategory
		};
	}

	public getCategoriesForLevel(socketId: string): string[] {
		const user = this.userRepository.findOne(socketId);

		const game = this.gameRepository.findOne(user.room);

		return game.level.randomlySelectedCategories;
	}

	public resolveAllUsersSelectedCategoryEnum(value: SelectedCategoryEnum): boolean {
		switch (value) {
			case SelectedCategoryEnum.all:
				return true;
			case SelectedCategoryEnum.no:
				return false;
		}
	}

	public isCategoryRoomReady(selectedCategoryDto: CategorySelectorDto): boolean {
		return !isNil(selectedCategoryDto.selectedCategory);
	}
}
