import { EventBus } from "@antyper/simple-event-bus";
import { inject, injectable } from "inversify";
import { Game } from "../../../domain/game/entity/Game";
import { GameCreatedEvent } from "../../../domain/game/event/GameCreatedEvent";
import { GameService } from "../../../domain/game/GameService";
import { GAME_TYPES } from "../../../domain/game/ioc/GameTypes";
import { LevelService } from "../../../domain/game/LevelService";
import { COMMON_TYPES } from "../../../infrastructure/ioc/CommonTypes";

@injectable()
export class CreateGameCommand {

	@inject(GAME_TYPES.GameService)
	private readonly gameService: GameService;

	@inject(GAME_TYPES.LevelService)
	private readonly levelService: LevelService;

	@inject(COMMON_TYPES.EventBus)
	private readonly eventBus: EventBus;

	public command(socketId: string): boolean {
		let game: Game = this.gameService.createGame(socketId);

		game.level = this.levelService.createLevel();
		game = this.gameService.updateGameState(game);

		this.eventBus.emitEvent(new GameCreatedEvent(game));

		return true;
	}
}
