import { IEventListener } from "@antyper/simple-event-bus";
import { Observable, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { GameCreatedEvent } from "../../../domain/game/event/GameCreatedEvent";

export class GameCreateEventHandler implements IEventListener<GameCreatedEvent, void> {
	private event: Subject<GameCreatedEvent>;

	getEventToHandle(): Observable<void> {
		return this.event.pipe(
				map(e => e.game),
				map(game => console.log(game))
		)
	}

	setEvent(event: Subject<GameCreatedEvent>): void {
		this.event = event;
	}

}
