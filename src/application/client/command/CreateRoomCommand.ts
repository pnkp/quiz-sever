import { inject, injectable } from "inversify";
import { RoomExistsException } from "../../../domain/client/exception/RoomExistsException";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { RoomRepository } from "../../../infrastructure/persist/client/RoomRepository";
import { CreateRoomCommandValidator } from "../validator/CreateRoomCommandValidator";

@injectable()
export class CreateRoomCommand {

	@inject(CLIENT_TYPES.RoomRepository)
	private readonly roomRepository: RoomRepository;

	@inject(CLIENT_TYPES.CreateRoomCommandValidator)
	private readonly createRoomCommandValidator: CreateRoomCommandValidator;

	public command(roomName: string): void {
		const room = this.roomRepository.findOne(roomName);

		this.createRoomCommandValidator.validate(roomName);

		if (room) {
			throw new RoomExistsException(roomName);
		}

		this.roomRepository.createRoom({
			room: roomName
		});

		console.log(`Room created: ${roomName}`);
	}

}
