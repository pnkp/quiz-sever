import { EventBus } from "@antyper/simple-event-bus";
import { inject, injectable } from "inversify";
import { PlayerCreatedEvent } from "../../../domain/client/event/PlayerCreatedEvent";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserService } from "../../../domain/client/UserService";
import { COMMON_TYPES } from "../../../infrastructure/ioc/CommonTypes";
import { UserEntity } from "../../../infrastructure/persist/client/entity/UserEntity";

@injectable()
export class CreatePlayerCommand {

	@inject(CLIENT_TYPES.UserService)
	private readonly userService: UserService;

	@inject(COMMON_TYPES.EventBus)
	private readonly eventBus: EventBus;

	public command(data: {socketId: string; room: string; playerName: string}): void {
		let { room, playerName } = data;
		room = room.trim().toLowerCase();

		const userEntity: UserEntity = {
			id: data.socketId,
			room,
			playerName
		};

		this.userService.addUserAndJoinToRoom(userEntity);

		this.eventBus.emitEvent(new PlayerCreatedEvent(userEntity));
	}
}
