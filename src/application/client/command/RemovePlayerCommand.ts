import { inject, injectable } from "inversify";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserService } from "../../../domain/client/UserService";
import { GameService } from "../../../domain/game/GameService";
import { GAME_TYPES } from "../../../domain/game/ioc/GameTypes";
import { GameRepository } from "../../../infrastructure/persist/game/GameRepository";

@injectable()
export class RemovePlayerCommand {

	@inject(CLIENT_TYPES.UserService)
	private readonly userService: UserService;

	@inject(GAME_TYPES.GameService)
	private readonly gameService: GameService;

	@inject(GAME_TYPES.GameRepository)
	private readonly gameRepository: GameRepository;

	public command(socketId: string): void {
		const user = this.userService.findUser(socketId);

		this.userService.removeUser(socketId);

		const game = this.gameRepository.findOne(user.room);

		if (!game) {
			return;
		}

		// this.gameService.removePlayerFromGame(game, socketId);
	}

}
