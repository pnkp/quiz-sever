import { IEventListener } from "@antyper/simple-event-bus";
import { Observable, Subject } from "rxjs";
import { map, mapTo, tap } from "rxjs/operators";
import { PlayerCreatedEvent } from "../../../domain/client/event/PlayerCreatedEvent";

export class PlayerCreatedEventHandler implements IEventListener<PlayerCreatedEvent, void> {
	private event: Subject<PlayerCreatedEvent>;

	getEventToHandle(): Observable<void> {
		return this.event.pipe(
				map((event$) => event$.player),
				tap((player) => console.log(`User ${player.playerName} (id#${player.id}) joined to room ${player.room} `)),
				mapTo(undefined)
		);
	}

	setEvent(event: Subject<PlayerCreatedEvent>): void {
		this.event = event;
	}

}
