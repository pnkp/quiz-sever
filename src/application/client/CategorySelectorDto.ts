import { SelectedCategoryEnum } from "../../domain/game/SelectedCategoryEnum";

export class CategorySelectorDto {
	selectedCategory?: string;
	allUsersSelectedCategory: SelectedCategoryEnum
}
