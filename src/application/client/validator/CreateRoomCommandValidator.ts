import { injectable } from "inversify";
import { EmptyRoomNameException } from "../../../domain/client/exception/EmptyRoomNameException";

@injectable()
export class CreateRoomCommandValidator {

	public validate(roomName: string): boolean {
		if (!roomName || roomName.trim().length === 0) {
			throw new EmptyRoomNameException()
		}

		return true;
	}

}
