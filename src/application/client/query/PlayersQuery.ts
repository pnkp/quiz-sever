import { inject, injectable } from "inversify";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserRepository } from "../../../infrastructure/persist/client/UserRepository";
import { PlayerDto } from "../PlayerDto";
import { PlayerNameDto } from "../PlayerNameDto";

@injectable()
export class PlayersQuery {
	@inject(CLIENT_TYPES.UserRepository)
	private readonly userRepository: UserRepository;

	public getPlayersInRoom(room: string): Array<PlayerNameDto> {
		room = room.trim().toLowerCase();

		const usersInRoom = this.userRepository.findUsersInRoom(room);

		return usersInRoom.map(u => {
			return {
				playerName: u.playerName
			}
		});
	}

	public findOne(socketId: string): PlayerDto {
		const {playerName, room, id} = this.userRepository.findOne(socketId);

		return {
			playerName,
			room,
			id
		};
	}

}

