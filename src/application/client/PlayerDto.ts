export class PlayerDto {
	public room: string;
	public playerName: string;
	public id: string;
}
