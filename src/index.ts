import "reflect-metadata";
import { EventBus } from "@antyper/simple-event-bus";
import express from "express";
import { Server } from "http";
import SocketIO from "socket.io";
import socketIO, { Socket } from 'socket.io';
import { COMMON_TYPES } from "./infrastructure/ioc/CommonTypes";
import { ContainerSingleton } from "./infrastructure/ioc/ContainerSingleton";
import { clientErrorHandler, errorHandler, logErrors } from "./infrastructure/middleware/handler/error/ErrorHandler";
import dotenv from "dotenv";
import cors from "cors";
import { categoryController } from "./ui/socket/category/CategoryController";
import { gameController } from "./ui/socket/game/GameController";
import { playerController } from "./ui/socket/player/PlayerController";

const container = ContainerSingleton.getInstance();
export const app = express();
dotenv.config();

const port = process.env.PORT || 5000;
app.set("port", port);

app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);
app.use(cors());

//routes for rest
import './ui/rest/game/GameController';

const http = new Server(app);
export const io: SocketIO.Server = socketIO(http);

const eventBus: EventBus = container.get(COMMON_TYPES.EventBus);

eventBus.handle();

const onConnect = (socket: Socket) => {
	playerController(socket);

	categoryController(socket);

	gameController(socket);
};

io.on("connection", onConnect);

http.listen(port, () => {
	console.log(`listening on *:${port}`);
});

