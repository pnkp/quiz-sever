import { injectable } from "inversify";
import { RoomEntity } from "./entity/RoomEntity";

@injectable()
export class RoomRepository {
	private readonly rooms: Array<RoomEntity> = [];

	public createRoom(roomEntity: RoomEntity): RoomEntity {
		this.rooms.push(roomEntity);

		return roomEntity;
	}

	public findOne(roomName: string): RoomEntity {
		return this.rooms.find(r => r.room === roomName);
	}
}
