import { injectable } from "inversify";
import { UserEntity } from "./entity/UserEntity";
import { UserWithCategoryEntity } from "./entity/UserWithCategoryEntity";

@injectable()
export class UserCategoryRepository {
	private readonly usersInCategory: Array<UserWithCategoryEntity> = [];

	public addUserToCategory(user: UserEntity, category: string): UserWithCategoryEntity {
		user.room = user.room.toLowerCase().trim();
		category = category.toLowerCase().trim();

		const userWithCategory: UserWithCategoryEntity = {
			user,
			category,
			room: user.room
		};

		this.usersInCategory.push(userWithCategory);

		return userWithCategory;
	}

	public findUsersInRoomWithSelectedCategory(room: string): Array<UserWithCategoryEntity> {
		room = room.toLowerCase().trim();
		return this.usersInCategory.filter(u => u.room === room);
	}

	public remove(id: string): void {
		const indexInUsersCategory = this.usersInCategory.findIndex(u => u.user.id === id);
		this.usersInCategory.splice(indexInUsersCategory, 1);
	}
}
