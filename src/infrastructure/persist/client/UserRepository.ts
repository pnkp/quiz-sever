import { injectable } from "inversify";
import { UserEntity } from "./entity/UserEntity";

@injectable()
export class UserRepository {
	private readonly users: Array<UserEntity> = [];

	public readonly random = Math.random();

	public add(userData: UserEntity): UserEntity {
		userData.room = userData.room.toLowerCase().trim();

		this.users.push(userData);

		return userData;
	}

	public remove(id: string): void {
		const indexInUsers = this.users.findIndex(u => u.id === id);
		this.users.splice(indexInUsers, 1);
	}

	public findUsersInRoom(room: string): UserEntity[] {
		room = room.trim();
		return this.users.filter(u => u.room === room)
	}

	public findOne(id: string): UserEntity {
		return this.users.find((u) => u.id === id);
	}

	public findAllExistsRooms(): string[] {
		return this.users.map(u => u.room);
	}
}
