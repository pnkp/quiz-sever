import { UserEntity } from "./UserEntity";

export class UserWithCategoryEntity {
	_id?: string;
	user: UserEntity;
	category: string;
	room: string;
}
