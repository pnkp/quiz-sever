export class UserEntity {
	_id?: string;
	playerName: string;
	id: string;
	room: string;
	ready?: boolean;
}
