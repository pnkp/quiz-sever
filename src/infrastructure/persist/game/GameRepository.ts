import { injectable } from "inversify";
import { GameEntity } from "./entity/GameEntity";

@injectable()
export class GameRepository {
	private readonly gameStates: GameEntity[] = [];

	public create(game: GameEntity) {
		this.gameStates.push(game);
		return game;
	}

	public findOne(room: string): GameEntity {
		return this.gameStates.find(g => g.room === room);
	}

	public update(game: GameEntity): GameEntity {
		const gameIndex: number = this.gameStates.findIndex(g => g.room === game.room);

		this.gameStates.splice(gameIndex, 1);
		this.gameStates.push(game);

		return game;
	}
}
