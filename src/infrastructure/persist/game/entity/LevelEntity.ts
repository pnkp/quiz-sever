import { UserEntity } from "../../client/entity/UserEntity";

export class LevelEntity {
	_id?: string;
	randomlySelectedCategories: string[];
	categorySelectedByUser?: Array<{user: UserEntity; category: string}>;
	levelFinished: boolean;
}
