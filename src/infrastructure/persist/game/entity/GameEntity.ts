import { UserEntity } from "../../client/entity/UserEntity";
import { LevelEntity } from "./LevelEntity";

export class GameEntity {
	public room: string;
	public users: UserEntity[];
	public level: LevelEntity;
}

