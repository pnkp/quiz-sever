import { injectable } from "inversify";
import { LevelEntity } from "./entity/LevelEntity";

@injectable()
export class LevelRepository {
	private readonly levels: Array<LevelEntity> = [];
	private index = 1;

	public create(level: LevelEntity): LevelEntity {
		level._id = this.index.toString();
		++this.index;
		this.levels.push(level);

		return level;
	}

	public update(level: LevelEntity): LevelEntity {
		const levelIndex = this.levels.findIndex(l => l._id === level._id);

		this.levels.splice(levelIndex, 1);
		this.levels.push(level);

		return level;
	}

	public findOne(id: string): LevelEntity {
		return this.levels.find(l => l._id === id);
	}
}
