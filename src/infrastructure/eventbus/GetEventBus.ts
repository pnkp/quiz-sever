import { EventBus } from "@antyper/simple-event-bus";
import { Container } from "inversify";
import { PlayerCreatedEventHandler } from "../../application/client/event/PlayerCreatedEventHandler";
import { GameCreateEventHandler } from "../../application/game/event/GameCreateEventHandler";
import { PlayerCreatedEvent } from "../../domain/client/event/PlayerCreatedEvent";
import { GameCreatedEvent } from "../../domain/game/event/GameCreatedEvent";

export const getEventBus = (container: Container): EventBus => {
	const eventBus = new EventBus();

	eventBus.addEvent(PlayerCreatedEvent);
	eventBus.addEventListener(
			PlayerCreatedEvent,
			new PlayerCreatedEventHandler()
	);

	eventBus.addEvent(GameCreatedEvent);
	eventBus.addEventListener(
			GameCreatedEvent,
			new GameCreateEventHandler()
	);

	return eventBus;
};
