import {NextFunction, Request, Response} from "express";


export const logErrors: (err: Error, req: Request, res: Response, next: NextFunction) => void
    = (err, req, res, next) => {
    console.error(err.stack);
    if (process.env.ENV === "DEV") {
        res.status(500).send({err});
        return;
    }
    next(err);
};

export const clientErrorHandler: (err: Error, req: Request, res: Response, next: NextFunction) => void
    = (err, req, res, next) => {
    if (req.xhr) {
        res.status(500).send({error: "Something failed!"});
    } else {
        next(err);
    }
};

export const errorHandler: (err: Error, req: Request, res: Response, next: NextFunction) => void
    = (err: Error, req: Request, res: Response, next: NextFunction) => {
    if (err) {
        res.status(500);
        res.send({error: "Something failed!"});
    }
    next();
};
