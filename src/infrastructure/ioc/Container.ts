import { EventBus } from "@antyper/simple-event-bus";
import { Container } from "inversify";
import { CreatePlayerCommand } from "../../application/client/command/CreatePlayerCommand";
import { CreateRoomCommand } from "../../application/client/command/CreateRoomCommand";
import { RemovePlayerCommand } from "../../application/client/command/RemovePlayerCommand";
import { CategorySelectorQuery } from "../../application/game/query/CategorySelectorQuery";
import { PlayersQuery } from "../../application/client/query/PlayersQuery";
import { CreateRoomCommandValidator } from "../../application/client/validator/CreateRoomCommandValidator";
import { CreateGameCommand } from "../../application/game/command/CreateGameCommand";
import { CategoryChooser } from "../../domain/game/CategoryChooser";
import { GameService } from "../../domain/game/GameService";
import { CATEGORY_TYPES } from "../../domain/game/ioc/CategoryTypes";
import { CLIENT_TYPES } from "../../domain/client/ioc/ClientTypes";
import { UserService } from "../../domain/client/UserService";
import { GAME_TYPES } from "../../domain/game/ioc/GameTypes";
import { LevelService } from "../../domain/game/LevelService";
import { getEventBus } from "../eventbus/GetEventBus";
import { RoomRepository } from "../persist/client/RoomRepository";
import { UserCategoryRepository } from "../persist/client/UserCategoryRepository";
import { UserRepository } from "../persist/client/UserRepository";
import { GameRepository } from "../persist/game/GameRepository";
import { LevelRepository } from "../persist/game/LevelRepository";
import { COMMON_TYPES } from "./CommonTypes";
import { ContainerSingleton } from "./ContainerSingleton";

export const getContainer: () => Container = () => {
	const container = new Container();

	container.bind(CLIENT_TYPES.UserRepository)
			.to(UserRepository)
			.inSingletonScope();

	container.bind(CLIENT_TYPES.UserCategoryRepository)
			.to(UserCategoryRepository)
			.inSingletonScope();

	container.bind(GAME_TYPES.GameRepository)
			.to(GameRepository)
			.inSingletonScope();

	container.bind(CLIENT_TYPES.RoomRepository)
			.to(RoomRepository)
			.inSingletonScope();

	container.bind(GAME_TYPES.LevelRepository)
			.to(LevelRepository)
			.inSingletonScope();

	container.bind(CLIENT_TYPES.UserService)
			.to(UserService);

	container.bind(CLIENT_TYPES.PlayersQuery)
			.to(PlayersQuery);

	container.bind(CLIENT_TYPES.CategorySelectorQuery)
			.to(CategorySelectorQuery);

	container.bind(CLIENT_TYPES.CreatePlayerCommand)
			.to(CreatePlayerCommand);

	container.bind(CLIENT_TYPES.CreateRoomCommand)
			.to(CreateRoomCommand);

	container.bind(CLIENT_TYPES.CreateRoomCommandValidator)
			.to(CreateRoomCommandValidator);

	container.bind(CLIENT_TYPES.RemovePlayerCommand)
			.to(RemovePlayerCommand);

	container.bind(CATEGORY_TYPES.CategoryChooser)
			.to(CategoryChooser);

	container.bind(GAME_TYPES.CreateGameCommand)
			.to(CreateGameCommand);

	container.bind(GAME_TYPES.LevelService)
			.to(LevelService);

	container.bind(GAME_TYPES.GameService)
			.to(GameService);

	container.bind<EventBus>(COMMON_TYPES.EventBus)
			.toDynamicValue(() => {
				return getEventBus(ContainerSingleton.getInstance());
			})
			.inSingletonScope();

	return container;
};
