import { Container } from "inversify";
import { getContainer } from "./Container";

export class ContainerSingleton {
	private static instance: Container;

	public static getInstance() {
		if (!this.instance) {
			this.instance = getContainer();
		}

		return this.instance;
	}
}
