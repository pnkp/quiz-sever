import { CategorySelectorQuery } from "../../../application/game/query/CategorySelectorQuery";
import { CATEGORY_TYPES } from "../../../domain/game/ioc/CategoryTypes";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserService } from "../../../domain/client/UserService";
import { GAME_TYPES } from "../../../domain/game/ioc/GameTypes";
import { app } from "../../../index";
import { ContainerSingleton } from "../../../infrastructure/ioc/ContainerSingleton";
import { GameRepository } from "../../../infrastructure/persist/game/GameRepository";
import { Request, Response } from "express";

const container = ContainerSingleton.getInstance();

app.get('/games', (req: Request, res: Response) => {
	const gameRepository: GameRepository = container.get(GAME_TYPES.GameRepository);
	const userService: UserService = container.get(CLIENT_TYPES.UserService);

	const player = userService.findUser(req.headers['player-id'] as string);

	res.send(gameRepository.findOne(player.room))
});

app.get('/rooms', (req: Request, res: Response) => {
	const userService: UserService = container.get(CLIENT_TYPES.UserService);

	res.send(userService.getAllUniqRooms());
});

app.get('/categories', (req: Request, res: Response) => {
	const categoriesSelectorQuery: CategorySelectorQuery = container.get(CLIENT_TYPES.CategorySelectorQuery);

	res.send(categoriesSelectorQuery.getCategoriesForLevel(req.headers['player-id'] as string));
});
