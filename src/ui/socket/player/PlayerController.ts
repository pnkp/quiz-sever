import { Socket } from "socket.io";
import { CreatePlayerCommand } from "../../../application/client/command/CreatePlayerCommand";
import { CreateRoomCommand } from "../../../application/client/command/CreateRoomCommand";
import { RemovePlayerCommand } from "../../../application/client/command/RemovePlayerCommand";
import { PlayersQuery } from "../../../application/client/query/PlayersQuery";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { io } from "../../../index";
import { ContainerSingleton } from "../../../infrastructure/ioc/ContainerSingleton";

const container = ContainerSingleton.getInstance();

export const playerController = (socket: Socket) => {

	socket.on("createRoom", (response: { room: string }, callback) => {
		try {
			const createRoomCommand: CreateRoomCommand = container.get(CLIENT_TYPES.CreateRoomCommand);
			createRoomCommand.command(response.room);

			socket.join(response.room);

			return callback(response.room);
		} catch (e) {
			return callback({error: e.message});
		}
	});

	socket.on("getPlayersInGame", () => {
		try {
			const playersQuery: PlayersQuery = container.get(CLIENT_TYPES.PlayersQuery);
			const player = playersQuery.findOne(socket.id);

			io.to(player.room).emit('playersInGame', playersQuery.getPlayersInRoom(player.room));
		} catch (e) {
			console.error(e)
		}
	});

	socket.on("join", (response: { room: string; playerName: string }, callback) => {
		try {
			const playerQuery: PlayersQuery = container.get(CLIENT_TYPES.PlayersQuery);
			const createPlayerCommand: CreatePlayerCommand = container.get(CLIENT_TYPES.CreatePlayerCommand);

			createPlayerCommand.command({
				...response,
				socketId: socket.id
			});

			const player = playerQuery.findOne(socket.id);
			socket.join(player.room);

			return callback({player});
		} catch (e) {
			console.error(e);
			return callback({error: e.message})
		}
	});

	socket.on("disconnect", () => {
		try {
			const playersQuery: PlayersQuery = container.get(CLIENT_TYPES.PlayersQuery);
			const playerDto = playersQuery.findOne(socket.id);

			const removePlayerCommand: RemovePlayerCommand = container.get(CLIENT_TYPES.RemovePlayerCommand);
			removePlayerCommand.command(socket.id);

			io.to(playerDto.room).emit('getConfirmationCounter', {isRunning: false, counter: undefined});
			io.to(playerDto.room).emit('playersInGame', playersQuery.getPlayersInRoom(playerDto.room));
		} catch (e) {
			console.error(e);
		}
	});

};
