import { Socket } from "socket.io";
import { CategorySelectorDto } from "../../../application/client/CategorySelectorDto";
import { CategorySelectorQuery } from "../../../application/game/query/CategorySelectorQuery";
import { UserWithCategory } from "../../../domain/client/entity/UserWithCategory";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { UserService } from "../../../domain/client/UserService";
import { io } from "../../../index";
import { ContainerSingleton } from "../../../infrastructure/ioc/ContainerSingleton";

const container = ContainerSingleton.getInstance();
const categorySelector: CategorySelectorQuery = container.get(CLIENT_TYPES.CategorySelectorQuery);
const userService: UserService = container.get(CLIENT_TYPES.UserService);

export const categoryController = (socket: Socket) => {
	socket.on("getCategories", () => {
		try {
			// const user = userService.findUser(socket.id);
			// io.to(user.room).emit('getCategories', categorySelector.getCategoriesForLevel())
		} catch (e) {
			console.error(e)
		}
	});

	socket.on("selectCategory", (selectedCategory) => {
		try {
			const user: UserWithCategory = userService.addUserToSelectedCategory(socket.id, selectedCategory);
			const selectedCategoryDto: CategorySelectorDto = categorySelector.getMostSelectedCategoryByUsers(user.room);

			io.to(user.room).emit("selectCategory", {
				isCategoryRoomReady: categorySelector.isCategoryRoomReady(selectedCategoryDto),
				allUsersSelectedCategory: categorySelector.resolveAllUsersSelectedCategoryEnum(selectedCategoryDto.allUsersSelectedCategory),
			});
		} catch (e) {

		}
	});
};


