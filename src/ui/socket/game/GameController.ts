import { EventBus } from "@antyper/simple-event-bus";
import { map } from "rxjs/operators";
import { Socket } from 'socket.io';
import { PlayersQuery } from "../../../application/client/query/PlayersQuery";
import { CreateGameCommand } from "../../../application/game/command/CreateGameCommand";
import { CLIENT_TYPES } from "../../../domain/client/ioc/ClientTypes";
import { GAME_TYPES } from "../../../domain/game/ioc/GameTypes";
import { io } from "../../../index";
import { ContainerSingleton } from "../../../infrastructure/ioc/ContainerSingleton";

const container = ContainerSingleton.getInstance();

export const gameController = (socket: Socket) => {

	socket.on('confirmGame', (response, callback) => {
		try {
			const createGameCommand: CreateGameCommand = container.get(GAME_TYPES.CreateGameCommand);

			if (createGameCommand.command(socket.id)) {
				return callback({confirmed: true});
			}
		} catch (e) {
			console.error(e)
		}
	});

	socket.on('runCounterSynchronisation', (response: {isRunning: boolean; counter: number}) => {
		try {
			const playersQuery: PlayersQuery = container.get(CLIENT_TYPES.PlayersQuery);
			const playerDto = playersQuery.findOne(socket.id);

			io.to(playerDto.room).emit('getConfirmationCounter', response);
		} catch (e) {
			console.error(e);
		}
	})

};
